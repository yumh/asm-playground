	section .note.openbsd.iden

	align 2
	dd 8, 4, 1
	db 'OpenBSD', 0
	dd 0
	align 2

	section .data

	status equ 0
	msg1 db "Process 2 died, good for him!", 10
	len1 equ $-msg1
	msg2 db "Hello, world! I'm process 2!", 10
	len2 equ $-msg2
	
	%define SYS_exit 1
	%define SYS_fork 2
	%define SYS_write 4
	%define SYS_wait4 11
	%define stdout 1
	
	section .text
	
	global _start
_start: mov rax, SYS_fork
	syscall
	
	cmp rax, -1
	je err			; fork failed
	cmp rax, 0
	je child		; jump to child code

	;; the father
	mov rdi, rax		; child' pid as first arg to wait4
	mov rsi, status		; pointer to status
	mov rdx, 0		; option
	mov r10, 0		; struct rusage *
	mov rax, SYS_wait4
	syscall
	
	;; write a message
	mov rax, SYS_write
	mov rdi, stdout
	mov rsi, msg1
	mov rdx, len1
	syscall
	
	jmp exit
	
err:	mov rax, SYS_exit
	mov rdi, 1
	syscall

child:	mov rax, SYS_write
	mov rdi, stdout
	mov rsi, msg2
	mov rdx, len2
	syscall
	
exit:	mov rax, SYS_exit
	xor rdi, rdi
	syscall
