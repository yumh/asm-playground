        section .note.openbsd.ident
        align 2
        dd 8, 4, 1
        db 'OpenBSD', 0
        dd 0
        align 2

	section .data

        %define SYS_exit 1

	;; fmt db "%d", 10, 0
	;; msg equ 5
	msg db "Hello, puts", 0

	section .text
	global main
	extern puts
main:	nop
	;; xor rax, rax
	;; mov rdi, fmt
	;; mov rsi, msg
	;; call printf
	mov rdi, msg
	call puts
	xor rax, rax
	ret

	;; mov rax, SYS_exit
	;; mov rdi, 0
	;; syscall
