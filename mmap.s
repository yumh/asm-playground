        section .note.openbsd.iden

        align 2
        dd 8, 4, 1
        db 'OpenBSD', 0
        dd 0
        align 2

        section .data

        devz db "/dev/zero", 0

        msg db "Hi!", 10
        msg_len equ $-msg

        %define SYS_exit        1
        %define SYS_write       4
        %define SYS_open        5
        %define SYS_close       6
        %define SYS_munmap      73
        %define SYS_mmap        197

        section .text
        global _start
_start: mov rax, SYS_open
        mov rdi, devz
        mov rsi, 0x02
        syscall

        mov rdi, 0              ; NULL, the system pics an address
        mov rsi, msg_len        ; sizeof
        mov rdx, 0x03           ; PROT_READ|PROT_WRITE
        mov r10, 0x02           ; MAP_PRIVATE
        mov r8, rax             ; fd
        mov r9, 0               ; offset
        mov rax, SYS_mmap
        syscall

        ;; save the pointer to the allocated memory
        mov r9, rax

        ;; close
        mov rax, SYS_close
        mov rdi, r8
        syscall

        ;; save the pointer to rsi
        mov rsi, r9
        mov rdx, msg

        ;; copy the string to the allocated buffer
        mov rdi, 0
loop:   cmp rdi, msg_len
        je end

        ;; copy a character from rdx to rsi, increment both pointer
        mov r10, [rdi + msg]
        mov [rsi + rdi], r10

        inc rdi
        jmp loop
end:
        mov rdi, 1
        mov rsi, r9
        mov rdx, msg_len
        mov rax, SYS_write
        syscall

        ;; unmap
        mov rax, SYS_munmap
        mov rdi, r9
        mov rsi, msg_len
        syscall

        ;; exit
exit:   mov rax, SYS_exit
        mov rdi, 0
        syscall
