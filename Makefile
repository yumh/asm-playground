fork.o: fork.s
	nasm -f elf64 -o fork.o fork.s

fork: fork.o
	ld -m elf_x86_64_obsd -o fork -nopie fork.o

mmap.o: mmap.s
	nasm -f elf64 -o mmap.o mmap.s

mmap: mmap.o
	ld -m elf_x86_64_obsd -o mmap -nopie mmap.o

server.o: server.s
	nasm -f elf64 -o server.o server.s

server: server.o
	ld -m elf_x86_64_obsd -o server -nopie server.o

printf.o: printf.s
	nasm -felf64 -o printf.o printf.s

printf: printf.o
	gcc -static printf.o -o printf
# ld -lc -m elf_x86_64_obsd -o printf -nopie printf.o
