        section .note.openbsd.iden

        align 2
        dd 8, 4, 1
        db 'OpenBSD', 0
        dd 0
        align 2

        section .data

        bind_err db "Error in bind(2)", 10
        bind_err_len equ $-bind_err

        msg db "Hi!", 10
        msg_len equ $-msg

        len equ 72

        ;; struct sockaddr_in
;; s:
;; sin_len:
;;      .byte 0
;; sin_family:
;;      .byte 0
;; sin_port:
;;      .word 0
;; sin_addr:
;;      .quadword 0
;; sin_zero:
;;      .quadword 0,0,0,0,0,0,0,0
;; sock:
;;      .quadword 0

s:      ISTRUC sockaddr_in
        AT sockaddr_in.sin_len, DB 1
        AT sockaddr_in.sin_family, DB 1
        AT sockaddr_in.port, DB 2
        AT sockaddr_in.addr, DB 4
        AT sockaddr_in.sin_zero, DB 64
        IEND

        %define SYS_exit 1
        %define SYS_write 4
        %define SYS_close 6
        %define SYS_socket 97
        %define SYS_bind 104
        %define SYS_listen 106
        %define SYS_accept 30
        %define stderr 2

        %define AF_INET 2
        %define SOCK_STREAM 1
        %define IPPROTO_TCP 6
        %define PORT 2586       ; htons(6666)

        section .text
        global _start
_start: nop                     ; so we can break in with gdb
        mov rax, SYS_socket
        mov rdi, AF_INET
        mov rsi, SOCK_STREAM
        mov rdx, IPPROTO_TCP
        syscall

        ;; save the master socket
        mov r14, rax

        ;; prepare the struct
        mov sin_family, AF_INET
        mov sin_addr, 0
        mov sin_port, PORT

        ;; bind
        mov rdi, rax            ; first argument to bind
        mov rsi, len
        mov rax, SYS_bind
        syscall

        cmp rax, -1
        je err

        ;; listen
while:  mov rax, SYS_listen
        mov rdi, r14
        mov rsi, 0
        syscall

        ;; accept
        mov rax, SYS_accept
        mov rsi, s
        mov rdx, len
        syscall

        ;; save client socket
        mov r15, rax

        ;; send
        mov rdi, rax
        mov rax, SYS_write
        mov rsi, msg
        mov rdx, msg_len
        syscall

        ;; close
        mov rax, SYS_close
        mov rdi, r15
        syscall
        jmp while

err:    mov rax, SYS_exit
        mov rdi, 1
        syscall
